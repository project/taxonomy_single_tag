<?php

/**
 * @file
 * Page callbacks for the taxonomy_single_tag module.
 */

/**
 * Helper function for autocompletion
 */
function taxonomy_single_tag_autocomplete($vid, $term_name = '') {
  // similar to taxonomy_autocomplete, but without taking care of commas
  // since a single value would be allowed
  
  $term_name = trim($term_name);
  $matches = array();
  if ($term_name != '') {
    $result = db_query_range(db_rewrite_sql("SELECT t.tid, t.name FROM {term_data} t WHERE t.vid = %d AND LOWER(t.name) LIKE LOWER('%%%s%%')", 't', 'tid'), $vid, $term_name, 0, 10);
    while ($tag = db_fetch_object($result)) {
      $matches[$tag->name] = check_plain($tag->name);
    }
  }
  drupal_json($matches);
}
